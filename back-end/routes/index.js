const express = require('express');

const userController = require('../controllers/user');
const loginController = require('../controllers/login');
const accountController = require('../controllers/account');
const movementController = require('../controllers/movement');
/*
const seg=require('../middlewares');
*/
const api = express.Router();

//USERS
api.get('/users',userController.getUsers);
api.get('/users/:id',userController.getUser);
api.post('/users',userController.saveUser);
api.put('/users/:id',userController.updateUser);
api.delete('/users/:id',userController.removeUser);

//LOGIN
api.post('/login',loginController.login);
api.post('/logout',loginController.logout);

//ACCOUNTS
api.get('/accounts',accountController.getAccounts);
api.get('/accounts/:id',accountController.getAccount);

//MOVIEENTS
api.get('/movements',movementController.getMovements);
api.get('/movements/:id',movementController.getMovement);

api.post('/users/:id/accounts',userController.saveUserAccount);
api.get('/users/:id/accounts',userController.getUserAccounts);
api.get('/users/:id/accounts/:idCuenta',userController.getUserAccount);

api.post('/users/:id/accounts/:idCuenta/movements',userController.saveUserAccountMovement);
api.get('/users/:id/accounts/:idCuenta/movements',userController.getUserAccountMovements);
api.get('/users/:id/accounts/:idCuenta/movements/:idMovimiento',userController.getUserAccountMovement);



//TOKEN
api.get('/seg',function (req,res){
  res.status(200).send({message:'ACCESO OK'});
});

api.get('/',function (req,res){
  res.status(200).send({message:'BIENVENIDOS A NUESTRA API'});
});

module.exports=api;
