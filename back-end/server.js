const app=require('./app');
const config=require('./config');

app.listen(config.port,()=>{
  console.log('TechU: Server started on port', config.port);
});
