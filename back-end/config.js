module.exports={
  port:process.env.PORT|| 3000,
  mlabHost:process.env.MLAB_HOST||'https://api.mlab.com/api/1/databases/',
  mlabDb:process.env.MLAB_DB||'apiperu/',
  mlapApiKey:process.env.MLAB_KEY||'apiKey=-Puknqeo7tEoHh5J9v4IgNoOLIiNhRuy',
  mlabCollectionUser:process.env.MLAB_COLLECTION_USER||'user',
  mlabCollectionAccount:process.env.MLAB_COLLECTION_ACCOUNT||'account',
  mlabCollectionMovement:process.env.MLAB_COLLECTION_MOVEMENT||'movement',
  URLbase:process.env.URL_BASE||'/apiperu/v0/',
}
