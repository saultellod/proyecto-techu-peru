var requestJSON = require('request-json');
var httpClient;

const config = require('../config');
const baseMlabURL = config.mlabHost+config.mlabDb+'collections/';


function login(req, res){
  var email = req.body.email
  var password = req.body.password
  //var queryString = 'q={"email":"' + email + '","password":"' + password + '"}'
  var queryString = 'q={"email":"' + email + '"}'
  httpClient = requestJSON.createClient(baseMlabURL);
  httpClient.get(config.mlabCollectionUser + '?' + queryString + "&l=1&" + config.mlapApiKey,
    function(errorGet, resGet, bodyGet){
      if (!errorGet) {
        if (bodyGet.length >= 1) {
          let cambio = '{"$set":{"logged":true}}'
          
          var claveBD = bodyGet[0].password;
          if (claveBD == password) {
            let queryString = 'q={"idCliente": ' + bodyGet[0].idCliente + '}&'
            httpClient = requestJSON.createClient(baseMlabURL);
            httpClient.put(config.mlabCollectionUser + '?' + queryString + config.mlapApiKey, JSON.parse(cambio),
            function(errorPut, respuestaMLabPut, bodyPut) {
              let cliente ={"idCliente": bodyGet[0].idCliente,
                              "nombre": bodyGet[0].nombre,
                              "apellido": bodyGet[0].apellido,
                              "email": bodyGet[0].email
                            }
              res.status(201).send({"msg": "Loggin correctamente",cliente });
             });

          } else {
            res.status(404).send({"msg":'Usuario o clave incorrectos'})

          }

        } else {
          res.status(404).send({"msg":'Usuario o clave incorrectos'})
        }
      }
    });
}

function logout(req, res){
  var queryString = 'q={"idCliente":' + req.body.idCliente + ',"logged":true}&'
  httpClient = requestJSON.createClient(baseMlabURL);
  httpClient.get(config.mlabCollectionUser + '?'+  queryString + config.mlapApiKey,
    function(errorGet, resGet, bodyGet){
      if (!errorGet) {
        if (bodyGet.length >= 1) {
          let cambio = '{"$set":{"logged":false}}'
          let queryString = 'q={"idCliente": ' + bodyGet[0].idCliente + '}&'
          httpClient = requestJSON.createClient(baseMlabURL);
          httpClient.put(config.mlabCollectionUser +'?' + queryString + config.mlapApiKey, JSON.parse(cambio),
           function(errorPut, respuestaMLabPut, bodyPut) {
            res.status(201).send({"msg": "logout correcto", bodyPut});
           });
        } else {
          res.status(404).send({"msg":'Usuario no logeado'})
        }
      }
    });
}

module.exports={
  login,
  logout
};
