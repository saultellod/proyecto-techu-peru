var requestJSON = require('request-json');
var httpClient;

const config = require('../config');
const baseMlabURL = config.mlabHost+config.mlabDb+'collections/';


//GET ALL USERS
function getMovements(req, res){
  httpClient = requestJSON.createClient(baseMlabURL);
  var queryString = 'f={"_id":0}&';
  httpClient.get(config.mlabCollectionMovement + '?' + queryString + config.mlapApiKey,
  function(err, resM, body) {
      if (!err) {
        res.status(200).send(body)
      } else {
        res.status(500).send({"msg": "Error consulta"})
      }
    });
}

function getMovement(req, res){
  var id = req.params.id;
  httpClient = requestJSON.createClient(baseMlabURL);
  var queryString = 'q={"idMovimiento":' + id +'}&f={"_id":0}&';
  httpClient.get(config.mlabCollectionMovement + '?' + queryString + config.mlapApiKey,
    function(err, resM, body) {
      if (resM.statusCode == 200) {
          if (body != null && body != '') {
            res.status(200).send(body[0]);
          } else {
            res.status(404).send({"msg": "Usuario no existe"});
          }
      } else {
        res.status(500).send({"msg": "Error consulta"})
      }
    });
}

module.exports={
  getMovements,
  getMovement
};
