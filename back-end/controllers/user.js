  var requestJSON = require('request-json');
  var httpClient;

  const config = require('../config');
  const baseMlabURL = config.mlabHost+config.mlabDb+'collections/';


  //GET ALL USERS
  function getUsers(req, res){
  httpClient = requestJSON.createClient(baseMlabURL);
  var queryString = 'f={"_id":0}&';
  httpClient.get(config.mlabCollectionUser + '?' + queryString + config.mlapApiKey,
  function(err, resM, body) {
      if (!err) {
        res.status(200).send(body)
      } else {
        res.status(500).send({"msg": "Error consulta"})
      }
    });
  }

  function getUser(req, res){
  var id = req.params.id;
  httpClient = requestJSON.createClient(baseMlabURL);
  var queryString = 'q={"idCliente":' + id +'}&f={"_id":0}&';
  httpClient.get(config.mlabCollectionUser + '?' + queryString + config.mlapApiKey,
    function(err, resM, body) {
      if (resM.statusCode == 200) {
          if (body != null && body != '') {
            res.status(200).send(body[0]);
          } else {
            res.status(404).send({"msg": "Usuario no existe"});
          }
      } else {
        res.status(500).send({"msg": "Error consulta"})
      }
    });
  }

  function saveUser(req, res){
  httpClient = requestJSON.createClient(baseMlabURL);
  var email = req.body.email;
  var queryStringConsulta = 'q={"email":"' + email + '"}'
  httpClient.get(config.mlabCollectionUser + '?' + queryStringConsulta + "&l=1&" + config.mlapApiKey,
    function(errConsulta, resConsulta, bodyConsulta) {
      if (resConsulta != null && resConsulta.statusCode == 200) {
        if (bodyConsulta != undefined
          && bodyConsulta != null
          && bodyConsulta != '' && bodyConsulta.length > 0) {
            res.status(404).send({"msg": "La cuenta " + req.body.email + ", ya existe"})
        } else {
          var orderBy = 's={"idCliente": -1}&'
          httpClient.get(config.mlabCollectionUser + '?' + orderBy + config.mlapApiKey,
            function(err, resM, body) {
              let usuario =body[0];
              let idmax = 0;
              if(usuario != null) {
                idmax = usuario.idCliente;
              }
              let newID = idmax+1;
              let newUser = {
                  "idCliente": newID,
                  "nombre": req.body.nombre,
                  "apellido": req.body.apellido,
                  "email": req.body.email,
                  "genero": req.body.genero,
                  "telefono": req.body.telefono,
                  "tipoDocumento": req.body.tipoDocumento,
                  "numeroDocumento": req.body.numeroDocumento,
                  "password": req.body.password
                }

                httpClient = requestJSON.createClient(baseMlabURL);
                httpClient.post(config.mlabCollectionUser + '?' + config.mlapApiKey,newUser,
                  function(errPost, resPost, bodyPost) {
                    if (!errPost) {
                      res.status(201).send({"msg": "Usuario añadido correctamente", bodyPost});
                    } else {
                      res.status(500).send({"msg": "Error resgistar Usuario"})
                    }
                  });
            });

        }
      } else {
        res.status(404).send({"msg": "Error validacion de usuario"})
      }
  });

  }


  function updateUser(req, res){
  httpClient = requestJSON.createClient(baseMlabURL);
  var cambio = '{"$set":' + JSON.stringify(req.body) + '}';
  var queryString = 'user?q={"idCliente": ' + req.params.id + '}&';
  httpClient.put(config.mlabCollectionUser + '?' + queryString + config.mlapApiKey, JSON.parse(cambio),
  function(errorPut, respuestaMLabPut, bodyPut) {
    res.status(201).send({"msg": "Usuario actualizado correctamente", bodyPut});
  });
  }

  function removeUser(req, res){
  var queryStringID = 'q={"idCliente":' + req.params.id + '}&';
  httpClient = requestJSON.createClient(baseMlabURL);
  httpClient.get(config.mlabCollectionUser + '?' + queryStringID + config.mlapApiKey,
    function(error, respuestaMLab, body){
      var respuesta = body[0];
      if (respuesta != null && respuesta != ""){
        httpClient.delete(config.mlabCollectionUser + "/" + respuesta._id.$oid +'?'+ config.mlapApiKey,
          function(error, respuestaMLab,body){
            res.send(body);
        });
      } else {
        res.status(404).send({"msg":'Usuario no encontrado'})
      }
    });
  }

  function getUserAccounts(req, res){
  var queryString ='q={"idCliente":' + req.params.id +'}&';
  httpClient = requestJSON.createClient(baseMlabURL);
  httpClient.get(config.mlabCollectionAccount + '?' + queryString + config.mlapApiKey,
  function(errGet, resGet, bodyGet) {
    var respuesta = bodyGet;
    res.send(respuesta);
  });
  }
  function getUserAccount(req, res){
  var queryString ='q={"idCliente":' + req.params.id +', "idCuenta":' +req.params.idCuenta+'}&';
  httpClient = requestJSON.createClient(baseMlabURL);
  httpClient.get(config.mlabCollectionAccount + '?' + queryString + config.mlapApiKey,
  function(errGet, resGet, bodyGet) {
    var respuesta = bodyGet;
    res.send(respuesta);
  });
  }


  function getUserAccountMovements(req, res){
  var queryString ='q={"idCuenta":' +req.params.idCuenta+'}&';
  httpClient = requestJSON.createClient(baseMlabURL);
  httpClient.get(config.mlabCollectionMovement + '?' + queryString + config.mlapApiKey,
  function(errGet, resGet, bodyGet) {
    var respuesta = bodyGet;
    res.send(respuesta);
  });
  }

  function getUserAccountMovement(req, res){
  var queryString ='q={"idCuenta":' +req.params.idCuenta+', "idMovimiento":' +req.params.idMovimiento+'}&';
  httpClient = requestJSON.createClient(baseMlabURL);
  httpClient.get(config.mlabCollectionMovement + '?' + queryString + config.mlapApiKey,
  function(errGet, resGet, bodyGet) {
    var respuesta = bodyGet;
    res.send(respuesta);
  });
  }


  function saveUserAccount(req, res){
  httpClient = requestJSON.createClient(baseMlabURL);
  var orderBy = 's={"idCuenta": -1}&'
  var n = /^[+-]?((\.\d+)|(\d+(\.\d+)?))$/;
  var importe = req.body.saldo
  var isnumber = n.test(importe);
  if (isnumber) {
    httpClient.get(config.mlabCollectionAccount + '?' + orderBy + config.mlapApiKey,
      function(err, resM, body) {
        let usuario =body[0];
        let idmax = 0;
        if(usuario != null) {
          idmax = usuario.idCuenta;
        }
        let newID = idmax+1;
        let newUserAccount = {
            "idCuenta": newID,
            "idCliente": req.body.idCliente,
            "monedaSaldo": req.body.monedaSaldo,
            "saldo": req.body.saldo,
            "tipo": req.body.tipo,
            "numeroCuenta": req.body.numeroCuenta
          }
          httpClient.post(config.mlabCollectionAccount + '?' + config.mlapApiKey,newUserAccount,
            function(errPost, resPost, bodyPost) {
              if (!errPost) {
                res.status(201).send({"msg":  req.body.tipo + " resgistrado correctamente", bodyPost});
              } else {
                res.status(500).send({"msg": "Error resgistar Usuario"})
              }
            });
      });
  } else {
    res.status(404).send({"msg": "Saldo invalido"})
  }

  }

  function saveUserAccountMovement(req, res){
  httpClient = requestJSON.createClient(baseMlabURL);

  var indicador = false;
  var objCuentaDestino = null
  var importeTransferencia = 0

  //restar saldo
  var n = /^[+-]?((\.\d+)|(\d+(\.\d+)?))$/;
  var importe = req.body.importe
  var isnumber = n.test(importe);
  //Validar Si es saldo valido
  if (isnumber) {
    var queryStringOK ='q={"idCliente":' + req.body.idCliente +', "idCuenta":' +req.body.idCuenta+'}&';
    httpClient = requestJSON.createClient(baseMlabURL);
    httpClient.get(config.mlabCollectionAccount + '?' + queryStringOK + config.mlapApiKey,
    function(errGet, resGet, bodyGet) {
      var cuentaOrigenOK = bodyGet[0]

      importeTransferencia = parseFloat(importe)
      var importeCuentaOrigen = parseFloat(cuentaOrigenOK.saldo)
      var importeCuentaOrigenFinal = importeCuentaOrigen - importeTransferencia

      if (importeCuentaOrigenFinal >= 0) {
        //validar cuenta Destino
        httpClient = requestJSON.createClient(baseMlabURL);
        var queryString_Consulta = 'q={"numeroCuenta":"' + req.body.cuentaDestino + '"}'
        httpClient.get(config.mlabCollectionAccount + '?' + queryString_Consulta + "&l=1&" + config.mlapApiKey,
          function(errConsulta, resConsulta, bodyConsulta) {
            if (bodyConsulta != undefined && bodyConsulta != null && bodyConsulta.length > 0) {

              objCuentaDestino = bodyConsulta[0]

              //Registra movimiento transferencia
              httpClient = requestJSON.createClient(baseMlabURL);
                      var orderBy = 's={"idMovimiento": -1}&'
                      httpClient.get(config.mlabCollectionMovement + '?' + orderBy + config.mlapApiKey,
                        function(err, resM, body) {
                          let usuario =body[0];
                          let idmax = 0;
                          if(usuario != null) {
                            idmax = usuario.idMovimiento;
                          }
                          var newID = idmax+1;

                          let newUserAccountMovement = {
                            "idMovimiento": newID,
                            "idCliente": req.body.idCliente,
                            "idCuenta": req.body.idCuenta,
                            "cuentaOrigen": req.body.cuentaOrigen,
                            "cuentaDestino": req.body.cuentaDestino,
                            "fecha": req.body.fecha,
                            "hora":req.body.hora,
                            "ubicacion": req.body.ubicacion,
                            "monedaImporte": req.body.monedaImporte,
                            "importe": "-" + req.body.importe,
                            "tipo": req.body.tipo,
                            "operacion": req.body.operacion
                            }
                            //Registra movimiento transferencia
                            httpClient = requestJSON.createClient(baseMlabURL);
                            httpClient.post(config.mlabCollectionMovement + '?' + config.mlapApiKey,newUserAccountMovement,
                              function(errPostMov, resPostMov, bodyPostMov) {
                                if (!errPostMov) {
                                  httpClient = requestJSON.createClient(baseMlabURL);
                                  var updCuentaOrigen = {
                                    "saldo": importeCuentaOrigenFinal
                                  }
                                  var cambioCuentaOrigen = '{"$set":' + JSON.stringify(updCuentaOrigen) + '}';
                                  var query_String = 'user?q={"idCuenta": ' + req.body.idCuenta + '}&';
                                  httpClient.put(config.mlabCollectionAccount + '?' + query_String + config.mlapApiKey, JSON.parse(cambioCuentaOrigen),
                                  function(errorPutCuentaOrigen, respuestaMLabPutCuentaOrigen, bodyPutCuentaOrigen) {
                                      res.status(201).send({"msg":  req.body.operacion + " resgistrado correctamente", bodyPostMov});
                                  });
                                } else {
                                  res.status(500).send({"msg": "Error Registrar movimientos"})
                                }
                              });
                        });

            } else {
              res.status(404).send({"msg": "Cuenta destino no existe"})
            }

          });

      } else {
        res.status(404).send({"msg": "Saldo cuenta origen insuficiente"})
      }

    });

  } else {
    res.status(404).send({"msg": "Monto invalido"});
  }

  }

  module.exports={
  getUsers,
  getUser,
  saveUser,
  updateUser,
  removeUser,
  getUserAccounts,
  getUserAccount,
  getUserAccountMovements,
  getUserAccountMovement,
  saveUserAccount,
  saveUserAccountMovement
  };
